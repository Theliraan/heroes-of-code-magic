﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace HeroesOfCodeMagic.Settings {
    [CreateAssetMenu(fileName = "DayNightCycle", menuName = "HeroesOfCodeMagic/DayNightCycle", order = 200)]
    public sealed class DayNightCycle : ScriptableObject {
        [Serializable]
        public struct Moment {
            [Range(1f, 3f)]
            public float Brightness;
            public Color Color;

            [Range(0f, 48f)]
            [SerializeField]
            private float hour;
            public float Hour => hour;

            [HideInInspector]
            public DateTime Date;

            public float GetTotalSeconds() => (float)new DateTime().AddHours(hour).TimeOfDay.TotalSeconds;
        }

        [Serializable]
        public struct Phase {
            [SerializeField]
            private Moment Start;
            [SerializeField]
            private Moment End;

            public Moment GetRandomMoment() {
                var lerp = Random.value;
                return new Moment {
                    Brightness = Mathf.Lerp(Start.Brightness, End.Brightness, lerp),
                    Date = new DateTime().AddSeconds(Mathf.Lerp(Start.GetTotalSeconds(), End.GetTotalSeconds(), lerp)),
                    Color = Color.Lerp(Start.Color, End.Color, lerp)
                };
            }
        }

        [Range(0f, 1f)]
        public float DayChance;

        [Range(0f, 20f)]
        public float LightSourceSpeed;
        
        public Phase Day;
        public Phase Night;

        public Phase GetRandomPhase() => Random.value >= DayChance ? Day : Night;
    }
}