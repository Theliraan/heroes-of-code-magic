﻿using System;
using UnityEngine;

namespace HeroesOfCodeMagic.Settings {
    public enum UnitType {
        Swordsman = 0,
        OrcWolfRider = 1,
        UndeadSwordsman = 2,
        StoneGolem = 3,
        Witch = 4,
        Sorserer = 5
    }

    public enum AbilityType {
        None = 0,
        Heal = 1,
        IncAttack = 2,
        IncHealth = 3
    }

    [CreateAssetMenu(fileName = "UnitsSettings", menuName = "HeroesOfCodeMagic/UnitsSettings", order = 1)]
    public sealed class UnitsSettings : ScriptableObject {
        [Serializable]
        public sealed class Unit {
            public UnitType Type;

            [Range(0, 100)]
            public uint Attack;

            [Range(0, 100)]
            public uint Health;

            public AbilityType Ability;

            public GameObject Prefab;

            public Sprite AbilityIcon;
        }

        public GameObject Tomb;

        public Unit[] Settings;
    }
}
