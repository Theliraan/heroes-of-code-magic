﻿using System;
using UnityEngine;

namespace HeroesOfCodeMagic.Settings {
    public enum TileType {
        Water = 0,
        Sand = 1,
        Rock = 2,
        Grass = 3,
    }

    [CreateAssetMenu(fileName = "TileSettings", menuName = "HeroesOfCodeMagic/TileSettings", order = 100)]
    public sealed class TileSettings : ScriptableObject {
        public const float MaxPassCost = 100f;

        [Serializable]
        public sealed class Tile {
            public TileType Type;

            [Range(0f, 100f)]
            public float PassCost;

            [Range(0f, 1f)]
            public float Height;

            public Material Material;
        }

        public Tile[] Settings;
    }
}