﻿using System;

public static class NumberExtention  {
    public static uint Clamp(this uint initial, uint limit) 
        => Math.Min(initial, limit);
    
    public static uint Clamp(this uint initial, uint min, uint max) 
        => initial < min ? min : initial > max ? max : initial;

    public static int Clamp(this int initial, int limit)
        => Math.Min(initial, limit);

    public static int Clamp(this int initial, int min, int max)
        => initial < min ? min : initial > max ? max : initial;
}