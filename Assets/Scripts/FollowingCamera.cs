﻿using UnityEngine;

namespace HeroesOfCodeMagic {
    public sealed class FollowingCamera : MonoBehaviour {
        public Transform Target;
        public float SmoothTime = 0.3f;
        public Vector3 Shift = new Vector3(-2, 4, -3);
        private Vector3 velocity = Vector3.zero;

        private void Start() => 
            transform.position = TargetPosition;

        private void Update() =>
            transform.position = Vector3.SmoothDamp(transform.position, TargetPosition, ref velocity, SmoothTime);
        
        private Vector3 TargetPosition => 
            Target.TransformPoint(Vector3.zero) + Shift;
    }
}