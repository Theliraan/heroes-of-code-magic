﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;
using HeroesOfCodeMagic.Model;

namespace HeroesOfCodeMagic {
    public sealed class GameController : MonoBehaviour {
        private enum Mode {
            Menu = 0,
            Map = 1,
            Battle = 2
        }

        public struct BattleParameters {
            public readonly Army Attackers;
            public readonly Army Defenders;

            public BattleParameters(Army attackers, Army defenders) {
                Attackers = attackers;
                Defenders = defenders;
            }
        }

        private static Mode CurrentMode {
            get { return (Mode) System.Enum.Parse(typeof(Mode), SceneManager.GetActiveScene().name); }
            set { SceneManager.LoadSceneAsync(value.ToString()); }
        }

        // TODO: inject
        public static Map Map { get; private set; }
        public static Army[] Armies { get; private set; }
        public static BattleParameters BattleStartParameters { get; private set; }

        private void Awake() {
            DontDestroyOnLoad(this);
            SetRandomSeed();
        }

        private static void SetRandomSeed() => Random.InitState(System.DateTime.UtcNow.Millisecond);

        public void StartSession() {
            Assert.IsTrue(CurrentMode == Mode.Menu);

            Map = new Map(40, 40);
            Armies = new[] {
                new Army(02, 02, FindObjectOfType<View.SquadEditor>().Squads),
                new Army(05, 07, Army.GenerateRandom()),
                new Army(10, 14, Army.GenerateRandom()),
                new Army(14, 33, Army.GenerateRandom()),
                new Army(26, 20, Army.GenerateRandom()),
                new Army(13, 33, Army.GenerateRandom()),
                new Army(38, 08, Army.GenerateRandom(1.3f, 1.4f)),
            };

            CurrentMode = Mode.Map;
        }
        
        public static void StartBattle(int attackerArmyIndex, int defenderArmyIndex) {
            Assert.IsTrue(CurrentMode == Mode.Map);

            BattleStartParameters = new BattleParameters(Armies[attackerArmyIndex], Armies[defenderArmyIndex]);

            CurrentMode = Mode.Battle;
        }

        public static void LeaveBattle() {
            Assert.IsTrue(CurrentMode == Mode.Battle);

            CurrentMode = Mode.Map;

            BattleStartParameters.Attackers.Rest();
            BattleStartParameters = default(BattleParameters);
        }

        public static void LeaveSession() {
            Assert.IsTrue(CurrentMode != Mode.Menu);

            Map = null;
            Armies = null;

            CurrentMode = Mode.Menu;
        }
    }
}