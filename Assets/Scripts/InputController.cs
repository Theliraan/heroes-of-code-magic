﻿using System;
using UnityEngine;

namespace HeroesOfCodeMagic {
    public sealed class InputController {
        private const float TapSecondsLimit = 0.2f;

        private float? inputStartTime = null;
        private Vector3? inputPreviousPosition = null;

        private event Action OnTap;
        private event Action OnDragStart;
        private event Action OnDragStop;
        private event Action<Vector3> OnDrag;
        private event Action<Vector2> OnScroll;

        public InputController(
            Action onTap = null, 
            Action onDragStart = null,
            Action onDragStop = null,
            Action<Vector3> onDrag = null,
            Action<Vector2> onScroll = null
         ) {
            OnTap += onTap;
            OnDrag += onDrag;
            OnDragStart += onDragStart;
            OnDragStop += onDragStop;
            OnScroll += onScroll;
        }

        public void Update() {
            if (Input.mouseScrollDelta.SqrMagnitude() > 0f) {
                OnScroll?.Invoke(Input.mouseScrollDelta);
            }

            if (Input.GetMouseButtonUp(0) && inputStartTime.HasValue) {
                var isTap = Time.time - inputStartTime <= TapSecondsLimit;
                var action = isTap ? OnTap : OnDragStop;
                action?.Invoke();

                inputStartTime = null;
                inputPreviousPosition = null;
            }
            else if (Input.GetMouseButton(0)) {
                if (!inputStartTime.HasValue) { 
                    inputStartTime = Time.time;
                }

                if (Time.time - inputStartTime > TapSecondsLimit) {
                    if (!inputPreviousPosition.HasValue) {
                        OnDragStart?.Invoke();
                        inputPreviousPosition = Input.mousePosition;
                    }
                    OnDrag?.Invoke(inputPreviousPosition.Value - Input.mousePosition);
                    inputPreviousPosition = Input.mousePosition;
                }
            }
        }
    }
}
