﻿namespace HeroesOfCodeMagic.Model {
    public sealed class Ability {
        public bool IsUsed { get; private set; }

        private System.Action onUse;

        public Ability(System.Action onUse) {
            this.onUse = onUse;
        }

        public bool Use() {
            if (IsUsed) { return false; }
            onUse();
            return IsUsed = true;
        }

        public void Reset() => IsUsed = false;
    }
}