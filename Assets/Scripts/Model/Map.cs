﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using HeroesOfCodeMagic.Settings;
using V2 = UnityEngine.Vector2Int;
using Random = UnityEngine.Random;

namespace HeroesOfCodeMagic.Model {
    public sealed class Map {
        public struct Tile {
            public readonly TileType Type;
            public readonly float PassCost;
            public readonly float Height;
            public readonly Material Material;

            public Tile(TileSettings.Tile tileSettings) {
                Type = tileSettings.Type;
                PassCost = tileSettings.PassCost;
                Height = tileSettings.Height;
                Material = tileSettings.Material;
            }
        }

        private readonly Tile[,] map;

        public Tile this[int x, int y] => map[x, y];
        public Tile this[V2 coords] => map[coords.x, coords.y];
        public int Width => map.GetLength(0);
        public int Height => map.GetLength(1);
        
        public Map(uint sizeX, uint sizeY) {
            map = GenerateMap(sizeX, sizeY);
        }

        private Tile[,] GenerateMap(uint sizeX, uint sizeY) {
            var newMap = new Tile[sizeX, sizeY];
            var tileSettings = Resources.Load<TileSettings>("TileSettings").Settings.ToDictionary(s => s.Type, s => s);

            var tileTypes = Enum.GetValues(typeof(TileType));
            var tileMaxNum = tileTypes.Length;

            var k = 4f + Random.value * 4f;

            for (var x = 0; x < sizeX; ++x) {
                for (var y = 0; y < sizeY; ++y) {
                    var noiseValue = Mathf.Clamp01(Mathf.PerlinNoise(x / k, y / k));
                    var type = (TileType)(noiseValue * tileMaxNum);
                    newMap[x, y] = new Tile(tileSettings[type]);
                }
            }

            return newMap;
        }

        #region A* Pathfinding

        public List<V2> BuildPath(V2 from, V2 to) {
            var cameFrom = new Dictionary<V2, V2>() { { from, from } };
            var costSoFar = new Dictionary<V2, float>() { { from, 0f } };

            V2 current;
            var frontier = new PriorityQueue<V2>();
            frontier.Enqueue(from, 0f);

            while (frontier.Count > 0f) {
                current = frontier.Dequeue();
                if (current.Equals(to)) break;

                foreach (var neighbor in Neighbors(current)) {
                    float newCost = costSoFar[current] + Cost(current, neighbor);

                    if (!costSoFar.ContainsKey(neighbor) || newCost < costSoFar[neighbor]) {
                        if (costSoFar.ContainsKey(neighbor)) {
                            costSoFar.Remove(neighbor);
                            cameFrom.Remove(neighbor);
                        }

                        costSoFar.Add(neighbor, newCost);
                        cameFrom.Add(neighbor, current);
                        float priority = newCost + Heuristic(neighbor, to);
                        frontier.Enqueue(neighbor, priority);
                    }
                }
            }

            var path = new List<V2>();
            current = to;
            while (!current.Equals(from)) {
                Assert.IsTrue(cameFrom.ContainsKey(current), "cameFrom does not contain current");
                path.Add(current);
                current = cameFrom[current];
            }

            path.Reverse();
            return path;
        }

        private static readonly V2[] Directions = new[] {
            new V2(-1, -1),
            new V2(-1, 0),
            new V2(-1, 1),
            new V2(0, -1),
            new V2(0, 1),
            new V2(1, -1),
            new V2(1, 0),
            new V2(1, 1),
        };

        private bool InBounds(V2 coords) =>
            (0 <= coords.x) && (coords.x < Width) && (0 <= coords.y) && (coords.y < Height);

        private bool Passable(V2 coords) => this[coords].PassCost < TileSettings.MaxPassCost;

        private float Cost(V2 a, V2 b) =>
            Heuristic(a, b) == 2f
                ? map[b.x, b.y].PassCost * Mathf.Sqrt(2f)
                : map[b.x, b.y].PassCost;

        private IEnumerable<V2> Neighbors(V2 coords) {
            foreach (var direction in Directions) {
                var next = new V2(coords.x + direction.x, coords.y + direction.y);
                if (InBounds(next) && Passable(next)) {
                    yield return next;
                }
            }
        }

        private static float Heuristic(V2 a, V2 b) => Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y);

        private class PriorityQueue<T> {
            private List<KeyValuePair<T, float>> elements = new List<KeyValuePair<T, float>>();

            public int Count => elements.Count;

            public void Enqueue(T item, float priority) => elements.Add(new KeyValuePair<T, float>(item, priority));

            // Returns the Location that has the lowest priority
            public T Dequeue() {
                int bestIndex = 0;

                for (int i = 0; i < elements.Count; i++) {
                    if (elements[i].Value < elements[bestIndex].Value) {
                        bestIndex = i;
                    }
                }

                T bestItem = elements[bestIndex].Key;
                elements.RemoveAt(bestIndex);
                return bestItem;
            }
        }

        #endregion
    }
}