﻿using System;
using System.Collections.Generic;
using System.Linq;
using HeroesOfCodeMagic.Settings;
using UnityEngine;

using Random = UnityEngine.Random;

namespace HeroesOfCodeMagic.Model {
    public sealed class Squad {
        public readonly Ability Ability;
        public readonly UnitType Type;

        private static Dictionary<UnitType, UnitsSettings.Unit> Settings;
        private uint initialCount;

        public float Cost => SingleAttack * SingleHealthMax * (Ability != null ? 1.8f : 1f);

        public uint SingleAttack { get; private set; }
        public uint SingleHealthMax { get; private set; }
        public uint CurrentHealth { get; private set; }

        public uint CurrentCount => (uint)Mathf.CeilToInt((float)CurrentHealth / SingleHealthMax);
        public bool IsAlive => CurrentHealth > 0u;
        public UnitsSettings.Unit UnitSettings => Settings[Type];
        public GameObject Tomb { get; private set; }

        public Squad(UnitType type, uint count) {
            // TODO: clean settings
            Settings = Settings ?? CreateSettings();
            Tomb = Resources.Load<UnitsSettings>("UnitsSettings").Tomb;
            Type = type;
            Ability = CreateAbility(UnitSettings.Ability);

            Rest(count);
        }

        public void Rest() => Rest(CurrentCount);

        private void Rest(uint count) {
            Ability?.Reset();

            initialCount = count;
            SingleAttack = UnitSettings.Attack;
            SingleHealthMax = UnitSettings.Health;
            CurrentHealth = count * SingleHealthMax;
        }

        private Dictionary<UnitType, UnitsSettings.Unit> CreateSettings() =>
            Resources.Load<UnitsSettings>("UnitsSettings").Settings.ToDictionary(u => u.Type, u => u);

        private Ability CreateAbility(AbilityType ability) {
            switch (ability) {
                case AbilityType.Heal: return new Ability(() => Heal(500u));
                case AbilityType.IncAttack: return new Ability(TwiceAttack);
                case AbilityType.IncHealth: return new Ability(TwiceHealth);
                case AbilityType.None: default: return null;
            }
        }

        public void Attack(Squad target) =>
            target.CurrentHealth -= (CurrentCount * SingleAttack).Clamp(target.CurrentHealth);

        private void Heal(uint amount) =>
            CurrentHealth = (CurrentHealth + amount).Clamp(initialCount * SingleHealthMax);

        private void TwiceAttack() => SingleAttack *= 2u;

        private void TwiceHealth() {
            SingleHealthMax *= 2u;
            CurrentHealth *= 2u;
        }

        public static Squad GenerateRandom(float countModifier, float qualityModifier) {
            var lastType = Enum.GetValues(typeof(UnitType)).Length - 1;
            var type = (UnitType)(Random.value * qualityModifier * lastType);
            var count = 1u + (uint)(Random.value * 100f * countModifier);

            return new Squad(type, count);
        }
    }
}