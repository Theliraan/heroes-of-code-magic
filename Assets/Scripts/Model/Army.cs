﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace HeroesOfCodeMagic.Model {
    public sealed class Army {
        public static uint MaxSquadCount = 6u;

        private readonly List<Squad> squadSlots;

        public Vector2Int Position { get; private set; }
        public List<Squad> Squads => squadSlots.Where(u => u?.IsAlive == true).ToList();
        public bool IsAlive => squadSlots.Any(u => u?.IsAlive == true);

        public Army(int x, int y, List<Squad> squads) {
            Position = new Vector2Int(x, y);
            squadSlots = squads;
        }

        public void GoTo(Vector2Int position) => Position = position;

        public void GoTo(int x, int y) => Position = new Vector2Int(x, y);

        public void Rest() => Squads.ForEach(s => s.Rest());

        public static List<Squad> GenerateRandom(
            float slotModifier = 1.0f,
            float countModifier = 1.0f,
            float qualityModifier = 1.0f
        ) {
            var squads = new List<Squad>();

            for (var s = 0; s < MaxSquadCount; ++s) {
                var needGenerate = s == 0 || Random.value < 0.4f * slotModifier;
                squads.Add(needGenerate ? Squad.GenerateRandom(countModifier, qualityModifier) : null);
            }

            return squads;
        }
    }
}