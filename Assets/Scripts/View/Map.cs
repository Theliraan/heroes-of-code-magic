﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

using DateTime = System.DateTime;
using DayNightCycle = HeroesOfCodeMagic.Settings.DayNightCycle;

namespace HeroesOfCodeMagic.View {
    public sealed class Map : MonoBehaviour {
        [SerializeField] private Light lightSource;
        [SerializeField] private Camera mainCamera;
        [SerializeField] private Text timeText;
        [SerializeField] private Text battleText;
        [SerializeField] private Sprite moveArrow;
        [SerializeField] private Sprite moveCross;

        private const float CameraMinDistance = 1f;
        private const float CameraMaxDistance = 20f;
        private const float DragSpeed = 0.2f;
        private const float ArmySizeModifier = 1.3f;
        private const float ArmyHeight = 1.15f;

        private GameObject[] armiesViews;
        private Queue<Vector2Int> path = new Queue<Vector2Int>();
        private Queue<GameObject> pointers = new Queue<GameObject>();
        private DateTime dayTime;
        private float startTime;

        private InputController inputController;

        private Tweener armyMoveTweener = null;
        private Tweener lightRotationTweener = null;

        private int tileLayer;
        private int tileLayerMask;

        private void Awake() {
            mainCamera = mainCamera ?? Camera.main;

            tileLayer = LayerMask.NameToLayer("MapTiles");
            tileLayerMask = LayerMask.GetMask("MapTiles");

            battleText.gameObject.SetActive(false);

            SetupInput();
        }

        private void SetupInput() {
            var followingCamera = mainCamera.GetComponent<FollowingCamera>();
            var right = mainCamera.transform.right;
            var left = new Vector3(mainCamera.transform.forward.x, 0f, mainCamera.transform.forward.z);

            inputController = new InputController(
                onTap: OnTap,
                onDragStart: () => followingCamera.enabled = false,
                onDragStop: () => followingCamera.enabled = true,
                onDrag: difference => {
                    var worldDifference = right * difference.x + left * difference.y;
                    mainCamera.transform.localPosition += worldDifference * DragSpeed;
                },
                onScroll: difference => {
                    var newSize = mainCamera.orthographicSize + difference.y;
                    mainCamera.orthographicSize = Mathf.Clamp(newSize, CameraMinDistance, CameraMaxDistance);
                }
            );
        }

        private void Start() {
            CreateMapObjects();
            CreateDayCycle();
            armiesViews = CreateArmies();

            mainCamera.GetComponent<FollowingCamera>().Target = armiesViews[0].transform;
        }

        private void OnDestroy() {
            armyMoveTweener?.Kill();
            armyMoveTweener = null;
            lightRotationTweener?.Kill();
            lightRotationTweener = null;
        }

        private GameObject[] CreateArmies() {
            var armies = new GameObject[GameController.Armies.Length];

            for (var a = 0; a < GameController.Armies.Length; a++) {
                var armyModel = GameController.Armies[a];

                GameObject armyView;
                if (armyModel.IsAlive) {
                    var prefab = armyModel.Squads
                        .OrderByDescending(s => s.Type)
                        .First()
                        .UnitSettings
                        .Prefab;
                    armyView = Instantiate(prefab);
                }
                else {
                    // not very cool, right?
                    armyView = Instantiate(Resources.Load<Settings.UnitsSettings>("UnitsSettings").Tomb); 
                }
                
                armyView.name = $"army_{a}";
                armyView.transform.SetParent(transform);
                armyView.transform.localPosition = new Vector3(armyModel.Position.x, ArmyHeight, armyModel.Position.y);
                armyView.transform.localEulerAngles = Vector3.up * Random.value * 360f;
                armyView.transform.localScale = Vector3.one * ArmySizeModifier;
                
                armies[a] = armyView;
            }

            return armies;
        }

        private void CreateDayCycle() {
            var cycleSettings = Resources.Load<DayNightCycle>("DayNightCycle");
            var moment = cycleSettings.GetRandomPhase().GetRandomMoment();
            lightSource.intensity = moment.Brightness;
            lightSource.color = moment.Color;

            var rotationX = Mathf.Lerp(25f, 50f, Random.value);
            var rotationY = Mathf.Lerp(-170f, 170f, Random.value);
            var rotationTarget = lightSource.transform.localEulerAngles + Vector3.down * 180f;
            var speed = 100f / cycleSettings.LightSourceSpeed;
            lightSource.transform.localEulerAngles = new Vector3(rotationX, rotationY, 0f);
            lightSource.transform
                .DOLocalRotate(rotationTarget, speed)
                .SetEase(Ease.Linear)
                .SetLoops(-1, LoopType.Incremental);

            mainCamera.backgroundColor = moment.Color;

            dayTime = moment.Date;
            startTime = Time.time - Random.value * 59f;
        }

        private List<GameObject> CreateMapObjects() {
            var map = GameController.Map;
            var newMapObjects = new List<GameObject>();

            for (var x = 0; x < map.Width; ++x) {
                for (var y = 0; y < map.Height; ++y) {
                    var type = map[x, y].Type;
                    var tile = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    tile.name = $"{x}_{y}_{type}";
                    tile.transform.SetParent(transform);
                    tile.transform.localPosition = new Vector3(x, map[x, y].Height, y);
                    tile.layer = tileLayer;
                    tile.GetComponent<Renderer>().material = map[x, y].Material;

                    newMapObjects.Add(tile);
                }
            }

            return newMapObjects;
        }

        private void Update() {
            if (armyMoveTweener != null) { return; }

            inputController.Update();
        }

        private void LateUpdate() {
            var currentTime = dayTime.AddSeconds(Time.time - startTime);
            var timeFormat = currentTime.Second % 2 == 0 ? "HH mm" : "HH:mm";
            timeText.text = currentTime.ToString(timeFormat);
        }

        private void OnTap() {
            RaycastHit hit;
            var ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, tileLayerMask)) {
                SelectMoveTarget(hit.transform.gameObject.name);
            }
        }

        private void SelectMoveTarget(string objName) {
            // Let's make some ugly coordinate parse
            var split = objName.Substring(0, objName.Length - 1).Split('_');
            if (split.Length < 3) { return; }

            var x = System.Convert.ToInt32(split[0]);
            var y = System.Convert.ToInt32(split[1]);
            var target = new Vector2Int(x, y);

            if (path.Any() && path.Last() == target) {
                MoveByPath();
            }
            else {
                path.Clear();
                GameController.Map
                    .BuildPath(GameController.Armies[0].Position, target)
                    .ForEach(p => path.Enqueue(p));

                pointers.ToList().ForEach(Destroy);
                pointers = CreateMovePointers(path.ToList());
            }
        }

        private void MoveByPath() {
            var childTransform = armiesViews[0].transform.GetComponentInChildren<Transform>();
            var moveTime = pointers.Count / 5f;
            TweenCallback lookToNext = () => childTransform.DOLookAt(pointers.Peek().transform.localPosition, 0.1f);
            
            armyMoveTweener = armiesViews[0].transform
                .DOLocalPath(pointers.Select(p => p.transform.localPosition).ToArray(), moveTime)
                .SetEase(Ease.InOutSine)
                .OnStart(lookToNext)
                .OnWaypointChange(w => {
                    Destroy(pointers.Dequeue());
                    if (pointers.Any()) { lookToNext(); }
                    OnWaypoint(w);
                })
                .OnComplete(() => armyMoveTweener = null);
        }

        private Queue<GameObject> CreateMovePointers(List<Vector2Int> pathPoints) {
            var movePointers = new Queue<GameObject>(pathPoints.Count);

            for (var p = 0; p < pathPoints.Count; ++p) {
                var pointer = new GameObject();
                var renderer = pointer.AddComponent<SpriteRenderer>();
                pointer.name = $"pointer_{p}";
                pointer.transform.SetParent(transform);
                pointer.transform.localPosition = new Vector3(pathPoints[p].x, ArmyHeight, pathPoints[p].y);
                if (p == pathPoints.Count - 1) {
                    renderer.sprite = moveCross;
                }
                else {
                    renderer.sprite = moveArrow;
                    pointer.transform.LookAt(new Vector3(pathPoints[p + 1].x, ArmyHeight, pathPoints[p + 1].y));
                }
                pointer.transform.localEulerAngles =
                    new Vector3(90f, pointer.transform.localEulerAngles.y, 0f);
                movePointers.Enqueue(pointer);
            }

            return movePointers;
        }

        private void OnWaypoint(int waypoint) {
            GameController.Armies[0].GoTo(path.Dequeue());
            for (var a = 1; a < GameController.Armies.Length; ++a) {
                if (GameController.Armies[a].IsAlive) {
                    var positionDiff = GameController.Armies[0].Position - GameController.Armies[a].Position;
                    positionDiff *= positionDiff;
                    if (positionDiff.x < 2 && positionDiff.y < 2) {
                        StartBattle(a);
                    }
                }
            }
        }

        private void StartBattle(int enemyArmyIndex) {
            armyMoveTweener.Pause();
            
            lightSource
                .DOIntensity(0f, 2f)
                .SetEase(Ease.InCubic);
                        
            battleText.gameObject.SetActive(true);
            var endColor = battleText.color;
            battleText.color = new Color(0f, 0f, 0f, 0f);
            battleText
                .DOColor(endColor, 3f)
                .SetEase(Ease.OutCubic)
                .OnComplete(() => GameController.StartBattle(0, enemyArmyIndex));
        }

        private void OnDrawGizmos() {
            if (!path.Any()) { return; }

            Gizmos.color = Color.green;
            path.ToList().ForEach(p => Gizmos.DrawWireCube(new Vector3(p.x, 1f, p.y), new Vector3(1f, 0.01f, 1f)));
        }
    }
}