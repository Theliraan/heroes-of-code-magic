﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

using SquadModel = HeroesOfCodeMagic.Model.Squad;
using Random = UnityEngine.Random;

namespace HeroesOfCodeMagic.View {
    public sealed class Squad : MonoBehaviour {
        [HideInInspector] public bool IsPlayerControllable;
        [HideInInspector] public Vector3 InitialEulerAngles;
        [HideInInspector] public SquadModel Model;
        [HideInInspector] public event Func<bool> CanUseAbility;

        [SerializeField] private Text countText;
        [SerializeField] private Canvas canvas;
        [SerializeField] private Button abilityButton;

        private GameObject prefab;

        private bool IsAbilityButton => IsPlayerControllable && Model.Ability?.IsUsed == false;

        private void Start() {
            canvas.worldCamera = Camera.main;

            prefab = Instantiate(Model.UnitSettings.Prefab, transform);
            prefab.transform.localEulerAngles = InitialEulerAngles;
            prefab.GetComponent<Animator>()?.SetFloat("idle_animation_shift", Random.value * 5f);

            UpdateAbility();
            UpdateState();
        }

        private void UpdateAbility() {
            if (IsAbilityButton) {
                abilityButton.gameObject.SetActive(true);
                abilityButton.GetComponent<Image>().sprite = Model.UnitSettings.AbilityIcon;
                abilityButton.onClick.RemoveAllListeners();
                abilityButton.onClick.AddListener(UseAbility);
            }
            else {
                abilityButton.gameObject.SetActive(false);
            }
        }

        private void UpdateState() {
            countText.text = Model.CurrentCount.ToString();

            if (Model.IsAlive) { return; }

            Instantiate(Model.Tomb, transform.parent).transform.localPosition = transform.localPosition;
            Destroy(gameObject);
        }

        private void UseAbility() {
            if (CanUseAbility?.Invoke() != true) { return; }

            Model.Ability.Use();
            abilityButton.gameObject.SetActive(IsAbilityButton);
            UpdateState();
        }

        public void Attack(Squad target, Action onAttackComplete) {
            var initialLookAt = prefab.transform.forward * 100f;
            var initialPosition = transform.position;
            var attackPosition = Vector3.Lerp(initialPosition, target.gameObject.transform.position, 0.8f);

            TweenCallback lookToEnemy = () => GetLookTweener(initialLookAt, () => onAttackComplete?.Invoke());
            TweenCallback goBack = () => GetMoveTweener(initialPosition, lookToEnemy);
            TweenCallback lookBack = () => GetLookTweener(initialPosition, goBack);
            TweenCallback waitAfterAttack = () => GetWaitTweener(lookBack);
            TweenCallback waitBeforeAttack = () => GetWaitTweener(() => {
                Model.Attack(target.Model);
                target.UpdateState();
                waitAfterAttack();
            });
            TweenCallback goToTarget = () => GetMoveTweener(attackPosition, waitBeforeAttack);
            TweenCallback lookToTarget = () => GetLookTweener(target.prefab.transform.position, goToTarget);

            lookToTarget();
        }

        private Tweener GetMoveTweener(Vector3 moveTarget, TweenCallback onComplete) => transform
                    .DOMove(moveTarget, 0.8f)
                    .SetEase(Ease.Flash)
                    .OnComplete(onComplete);

        private Tweener GetWaitTweener(TweenCallback onComplete) => prefab.transform
                    .DOLookAt(transform.forward, 0.2f)
                    .OnComplete(onComplete);

        private Tweener GetLookTweener(Vector3 lookTarget, TweenCallback onComplete) => prefab.transform
                    .DOLookAt(lookTarget, 0.3f)
                    .OnComplete(onComplete);

        public void Select() => SetToAllMaterials(this, ActivateMaterialHighlight);

        public void Deselect() => SetToAllMaterials(this, DeactivateMaterialHighlight);

        private static void SetToAllMaterials(Squad squad, System.Action<Material> action) =>
            squad
                .GetComponentsInChildren<Renderer>()
                .Select(r => r.material)
                .ToList()
                .ForEach(action);

        private static void ActivateMaterialHighlight(Material material) =>
            material
            .DOColor(Color.gray, 0.3f)
            .SetEase(Ease.Linear)
            .SetLoops(-1, LoopType.Yoyo)
            .SetRelative(true);

        private static void DeactivateMaterialHighlight(Material material) {
            material.DORewind();
            material.DOKill();
        }
    }
}