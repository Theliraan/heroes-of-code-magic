﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace HeroesOfCodeMagic.View {
    public sealed class Battle : MonoBehaviour {
        [SerializeField] private GameObject squadPrefab;
        [SerializeField] private Camera mainCamera;
        [SerializeField] private Text turnText;
        [SerializeField] private Image turnTextBackground;

        private InputController inputController;

        private bool isPlayerTurn;
        private bool isPending;

        private Squad attackedSquad = null;
        private Squad AttackedSquad {
            get { return attackedSquad; }
            set {
                SelectSquad(value, attackedSquad);
                attackedSquad = value;
            }
        }

        private Squad attackingSquad = null;
        private Squad AttackingSquad {
            get { return attackingSquad; }
            set {
                SelectSquad(value, attackingSquad);
                attackingSquad = value;
            }
        }

        private int squadLayer;
        private int squadLayerMask;

        private List<Squad> attackers; // For bot targeting
        private List<Squad> defenders; // For bot targeting
        private int attackersSquadIndex = -1;
        private int defendersSquadIndex = -1;

        private void Awake() {
            mainCamera = mainCamera ?? Camera.main;

            squadLayer = LayerMask.NameToLayer("BattleSquad");
            squadLayerMask = LayerMask.GetMask("BattleSquad");

            turnText.DOFade(0f, 0f);
            inputController = new InputController(OnTap);
            isPlayerTurn = Random.value >= 0.5f;
            isPending = true;

            attackers = CreateSquads(GameController.BattleStartParameters.Attackers, true);
            defenders = CreateSquads(GameController.BattleStartParameters.Defenders, false);
        }

        private void Start() => NextTurn();

        private List<Squad> CreateSquads(Model.Army army, bool isPlayer) {
            var squads = new List<Squad>();
            var s = -1;

            foreach (var squadModel in army.Squads) {
                s++;
                var cachedS = s;
                var namePrefix = isPlayer ? "Player" : "Enemy";
                var squadView = Instantiate(squadPrefab, transform);
                squadView.name = $"{namePrefix}_{squadModel.Type}_{s}";
                squadView.layer = squadLayer;
                squadView.transform.localPosition = new Vector3(s * 2f, 0f, isPlayer ? -5f : 5f);

                var squadComponent = squadView.GetComponent<Squad>();
                squadComponent.Model = squadModel;
                squadComponent.IsPlayerControllable = isPlayer;
                squadComponent.InitialEulerAngles = isPlayer ? Vector3.zero : (Vector3.up * 180f);
                squadComponent.CanUseAbility += () => {
                    if (isPlayer != isPlayerTurn) { return false; }
                    if (isPlayer && cachedS != attackersSquadIndex) { return false; }
                    if (!isPlayer && cachedS != defendersSquadIndex) { return false; }
                    return true;
                };

                squads.Add(squadComponent);
            }

            return squads;
        }

        private void Update() {
            if (isPending || !isPlayerTurn) { return; }

            inputController.Update();
        }

        private void OnTap() {
            RaycastHit hit;
            var ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, squadLayerMask)) {
                var target = hit.transform.gameObject.GetComponent<Squad>();
                if (target?.IsPlayerControllable == true) { return; }

                if (AttackedSquad == target && AttackingSquad != null && AttackedSquad != null) { // attack target
                    isPending = true;
                    AttackingSquad.Attack(AttackedSquad, NextTurn);
                    AttackingSquad = null;
                    AttackedSquad = null;
                }
                else { // select new target
                    AttackedSquad = target;
                }
            }
            else { // deselect
                AttackedSquad = null;
            }
        }

        private void NextTurn() {
            isPending = false;

            if (!GameController.BattleStartParameters.Defenders.IsAlive) {
                GameController.LeaveBattle();
                return;
            }

            if (!GameController.BattleStartParameters.Attackers.IsAlive) {
                GameController.LeaveSession();
                return;
            }

            isPlayerTurn = !isPlayerTurn;

            TweenCallback onTurn = isPlayerTurn
                ? (TweenCallback)(() => AttackingSquad = SelectNextSquad(ref attackersSquadIndex, attackers))
                : (TweenCallback)AIAttack;
            ShowTurnText(isPlayerTurn, onTurn);
        }

        private void AIAttack() {
            isPending = true;
            var random = new System.Random();
            var liveTargets = attackers.Where(u => u != null && u.Model.IsAlive).ToArray();
            var randomTarget = liveTargets.ElementAt(random.Next(0, liveTargets.Length));
            SelectNextSquad(ref defendersSquadIndex, defenders).Attack(randomTarget, NextTurn);
        }

        private Squad SelectNextSquad(ref int lastSquadIndex, List<Squad> squads) {
            Squad squad = null;
            while (squad == null) {
                ++lastSquadIndex;
                if (lastSquadIndex == squads.Count) {
                    lastSquadIndex = 0;
                }
                squad = squads[lastSquadIndex];
            }
            return squad;
        }

        private void ShowTurnText(bool isPlayerTurn, TweenCallback onComplete) {
            turnText.text = isPlayerTurn ? "YOUR TURN" : "ENEMY TURN";
            turnText.DORewind();
            turnText.DOKill();

            TweenCallback disappear = () => turnText.DOFade(0f, 0.3f).SetEase(Ease.OutCubic).OnComplete(() => onComplete?.Invoke());
            TweenCallback wait = () => turnText.DOFade(1f, 1f).OnComplete(disappear);
            TweenCallback appear = () => turnText.DOFade(1f, 0.3f).SetEase(Ease.InCubic).OnComplete(wait);

            appear();

            turnTextBackground.DORewind();
            turnTextBackground.DOKill();
            turnTextBackground.fillAmount = 0f;

            TweenCallback fillBack = () => turnTextBackground.DOFillAmount(0f, 0.3f);
            TweenCallback fillWait = () => turnTextBackground.DOFillAmount(1f, 1f).OnComplete(fillBack);
            TweenCallback fill = () => turnTextBackground.DOFillAmount(1f, 0.3f).OnComplete(fillWait);

            fill();
        }

        private void SelectSquad(Squad newSquad, Squad oldSquad) {
            if (newSquad == null) {
                oldSquad?.Deselect();
            }
            else if (newSquad == oldSquad) {
                newSquad.Deselect();
            }
            else {
                oldSquad?.Deselect();
                newSquad.Select();
            }
        }
    }
}