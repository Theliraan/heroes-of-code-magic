﻿using UnityEngine;
using UnityEngine.UI;
using HeroesOfCodeMagic.Settings;

namespace HeroesOfCodeMagic.View {
    public sealed class SquadEdit : MonoBehaviour {
        [SerializeField] private Transform preview;
        [SerializeField] private Text unitName;
        [SerializeField] private Text health;
        [SerializeField] private Text attack;
        [SerializeField] private Text count;
        [SerializeField] private Text ability;
        [SerializeField] private Button minus;
        [SerializeField] private Button plus;

        [HideInInspector] public UnitsSettings.Unit Unit;
        [HideInInspector] public uint Count;

        private void Start() {
            // Preview
            Instantiate(Unit.Prefab, preview).transform.localEulerAngles = Vector3.up * 180f;

            unitName.text = Unit.Type.ToString();
            health.text = "Health: " + Unit.Health;
            attack.text = "Attack: " + Unit.Attack;

            // Ability
            if (Unit.Ability != AbilityType.None) {
                ability.enabled = true;
                ability.text = "Ability: " + Unit.Ability.ToString();
            }
            else {
                ability.enabled = false;
            }

            // Count
            Count = (uint)(Random.value * 100f);
            ChangeCount();

            minus.onClick.RemoveAllListeners();
            minus.onClick.AddListener(() => ChangeCount(-1));

            plus.onClick.RemoveAllListeners();
            plus.onClick.AddListener(() => ChangeCount(+1));
        }

        private void ChangeCount(int delta = 0) {
            Count = (uint)((int)Count + delta).Clamp(0, 100);
            count.text = "Count: " + Count;
        }
    }
}