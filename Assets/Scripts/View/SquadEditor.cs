﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using HeroesOfCodeMagic.Settings;

namespace HeroesOfCodeMagic.View { 
    public class SquadEditor : MonoBehaviour {
        [SerializeField] private GameObject squadEdit;

        private readonly List<SquadEdit> squadEdits = new List<SquadEdit>();

        private void Start() {
            var units = Resources.Load<UnitsSettings>("UnitsSettings").Settings;

            foreach(var unit in units) { 
                var squadEditObj = Instantiate(squadEdit, transform);
                var squadEditComp = squadEditObj.GetComponent<SquadEdit>();
                squadEditComp.Unit = unit;
                squadEdits.Add(squadEditComp);
            }
        }

        private void OnDisable() {
            squadEdits.ForEach(Destroy);
            squadEdits.Clear();
        }

        public List<Model.Squad> Squads => squadEdits
            .Where(s => s.Count > 0)
            .Select(s => new Model.Squad(s.Unit.Type, s.Count))
            .ToList();
    }
}